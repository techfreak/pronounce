//https://www.dict.cc/?s=recording
//search for idArr
//get first element
//https://audio.dict.cc/speak.audio.v2.php?error_as_text=1&type=mp3&id=10560&lang=en_rec_ip&lp=DEEN
extern crate reqwest;
extern crate rodio;
use std::convert::TryInto;
use std::io::BufReader;
use rodio::Source;
use std::fs::File;
use std::io::prelude::*;
use std::{time,env};
//use reqwest::{Result,Error};

fn get_dict_entry(name:&str)->Result<std::string::String, reqwest::Error>  {
    let url = format!("https://www.dict.cc/?s={}",name);
    return  reqwest::get(&url)?
        .text();
}
fn get_mp3(id:&str)-> std::result::Result<reqwest::Response, reqwest::Error>{
    let url = format!("https://audio.dict.cc/speak.audio.v2.php?error_as_text=1&type=mp3&id={}&lang=en_rec_ip&lp=DEEN",id);
    return  reqwest::get(&url);
}
fn filter(raw:&str) -> &str
{
    let mut res = "";
    let pattern_id:usize =  match raw.rfind("idArr"){
        None => {0},
        Some(x) => {x}};
    if pattern_id != 0 {
        let filtered_text = &raw[pattern_id..raw.len()];
        let pattern_newline:usize = filtered_text.rfind("c1Arr").unwrap().try_into().unwrap();
        res =&filtered_text [20..pattern_newline-7];
    }
    return res;
}
fn main()  {
    let args: Vec<String> = env::args().collect();
    if args.len() <= 1  {println!("You have to input a word.")}
    else   {

        let html:std::string::String = get_dict_entry(&args[1]).unwrap();
        //println!("{:?}",html);
        let filtered_text = filter(&html);
        if filtered_text != ""{
            let ids: Vec<&str> =filtered_text.split(",").collect();
            //download mp3 and save to file
            let mut buf: Vec<u8> = vec![];
            let mut download = get_mp3(ids[0]).unwrap();
           // println!("{:?}",download);
            download.copy_to(&mut buf).unwrap();
            let mut file = File::create("/tmp/temp.mp3").unwrap();
            file.write_all(&buf).unwrap();
            file = File::open("/tmp/temp.mp3").unwrap();
            //play mp3
            let device = rodio::default_output_device().unwrap();
            let source = rodio::Decoder::new(BufReader::new(file)).unwrap();
            rodio::play_raw(&device, source.convert_samples());
            //println!("{:?}",ids[0]);
            let dur = time::Duration::from_millis(2500);
            std::thread::sleep(dur);
        }
        else { println!("It seems you have  made a typo in your word because it could not be found"); }
    }}
