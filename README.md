# pronounce 

Pronounce is a cli application that returns the correct spelling of a given word.

## Getting Started

These instructions will help you to build the application.

### Prerequisites
* [rust](https://www.rust-lang.org/tools/install)                    


### How to use it in your project

1.First git clone this repository in to your development directory:
```
git clone https://gitlab.com/techfreak/pronounce
```

2.And run the command
```
cd pronounce
cargo run
```

3.Profit !


### Alternatives

## Authors

* techfreak


## License

This project is licensed under the GPLv2 - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

